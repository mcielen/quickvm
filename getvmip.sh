#!/bin/bash
virsh domifaddr $1 | grep -oE "([0-9]{1,3}\.){3}[0-9]{1,3}"
